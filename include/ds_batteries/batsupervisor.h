/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 8/22/19.
//

#ifndef PROJECT_BATSUPERVISOR_H
#define PROJECT_BATSUPERVISOR_H
#include "batsupervisorobject.h"
#include "batsupervisorsequence.h"
#include "ds_hotel_msgs/BatMan.h"
#include "ds_base/ds_process.h"
#include "ds_hotel_msgs/ClioChgCmd.h"
#include "ds_hotel_msgs/PowerCmd.h"
#include "ds_hotel_msgs/ClioBattery.h"
#include "ds_hotel_msgs/PowerSupply.h"

namespace ds_batteries
{
    class BatSupervisor : public ds_base::DsProcess
    {
    public:
        explicit BatSupervisor();

        BatSupervisor(int argc, char* argv[], const std::string& name);

        ~BatSupervisor() override;
        DS_DISABLE_COPY(BatSupervisor)

    private:
        void tick(const ros::TimerEvent&);

        //MultichargeSequence* multi;
        ChargeSequence* manager;

        int num_bats;
        int num_chgs;

        std::vector<Bat*> bats;
        std::vector<Pwr*> chgs;
        Pwr* shore;

        ros::Publisher batsupervisor_pub_;
        ds_hotel_msgs::BatMan latest;
        void pubLatest(const ros::TimerEvent&);
        ros::Timer pub_timer;

        std::vector<ros::Subscriber> bat_subs_;
        std::vector<ros::ServiceClient> bat_clients_;

        std::vector<ros::Subscriber> chg_subs_;
        std::vector<ros::ServiceClient> chg_clients_;

        ros::Subscriber shore_sub_;
        ros::ServiceClient shore_client_;

        ros::ServiceServer chg_cmd_;
        ros::ServiceServer bat_pwr_cmd_;
        ros::ServiceServer shore_pwr_cmd_;

        ros::Timer multi_timer;

        bool _chg_cmd(const ds_hotel_msgs::ClioChgCmd::Request& req, const ds_hotel_msgs::ClioChgCmd::Response& resp);

        bool _bat_pwr_cmd(const ds_hotel_msgs::PowerCmd::Request& req, const ds_hotel_msgs::PowerCmd::Response& resp);

        bool _shore_pwr_cmd(const ds_hotel_msgs::PowerCmd::Request& req, const ds_hotel_msgs::PowerCmd::Response& resp);

        void startCharge();

        void stopCharge();

        void batsOn();

        void batsOff();

        void shoreOn();

        void shoreOff();

        void safeBats();  // automatically called when this object is destroyed
        void stop();      // stop the driver

    protected:
        /// \brief overwritten functions from DsProcess
        void setupParameters() override;

        void setupConnections() override;

        ChargeConfig config;
    };
}
#endif //PROJECT_BATSUPERVISOR_H
