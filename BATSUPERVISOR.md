# Battery Supervisor
Designed for managing multiple batteries and charging. 
## Objects/Actions
The battery supervisor instantiates a number of objects at startup and at run and enables several different actions.

### Objects at Startup
#### Bat
Purpose: Subscribes to and tracks latest information for each connected battery

#### Chg
Purpose: Subscribes to and maintains latest charge information for each connected charger

#### Pwr
Purpose: Stores location and latest information for connected shore power supply

### Objects at Run
#### ChargeSequence
Purpose: Tracks the state of charge and updates after a set period of time. Adjusts the charger's voltage and current limits in response to current charge state.

### Actions
#### Start Charge
Creates a ChargeSequence object. manages charge updating

#### Stop Charge
Sets each battery state to off and destroys any existing ChargeSequence objects

#### Shore On/Off
Sends the appropriate commands to the connected shore power supply

## ROS Interactions
### Service Servers
chg_cmd_ // ds_hotel_msgs::ClioChgCmd 

bat_pwr_cmd_ // ds_hotel_msgs::PowerCmd

shore_pwr_cmd_ // ds_hotel_msgs::PowerCmd
### Subscriptions
bat_subs

chg_subs

shore_sub
### Publishers
batsupervisor_pub_ // ds_hotel_msgs::BatMan

## Charge Sequence Logic
The charge command is resent every 40 seconds and the state of the charge is evaluated at this time for any changes. Based on preconfigured charge settings, the charge sequence enters different states. These states are: POWERUP, RAMP, CHARGE, and DONE. There are also periodic checks for battery errors or comms timeouts during the charge.

### POWERUP State
Handles the charge startup and makes sure batteries are charging before transferring to RAMP state
### RAMP State
Ramps charge up to constant voltage
### CHARGE state
Normal charging state until current or measured amps from charger <= preconfigured current shutoff numbers 
### DONE state
Charge is done, do nothing

## BASIC CHARGE SEQUENCE EXPECTED BEHAVIOR
Below is the expected behavior of a charge sequence

1. Click batterycharge button in gui or rosservice call with a `ds_hotel_msgs::ClioChgCmd::Request::CHARGE_CMD_CHARGE`

2. BatSupervisor::startCharge() is called

3. A new ChargeSequence is created with the chg config, batteries, current charger, and a ROS timer

4. ChargeSequence starts a timer, sets initial configuration parameters, sets chargeState to POWERUP and then calls ChargeSequence::startCharge()

5. ChargeSequence::startCharge() sets all batteries off, calls Bat::setCharge(), and sets target voltage and initial current on the connected charger
   
      * Bat::setCharge() sends a rosservice call to put each battery in charge state

6. The timer callback is set to ChargeSequence::tick() function

7. The ChargeSequence::tick() function checks for charge_notOk() flag and calls stopCharge() if flag is true
   
      * ChargeSequence::charge_notOk() polls each battery and charger for error flags or commsTimeouts
   
      * ChargeSequence::stopCharge() cycles through each battery and calls Bat::setOff() which sends a rosservice call to turn bat off. It also turns off the charger and sets the charge state to DONE

8. The ChargeSequence::tick() function manages between state switching and calls the appropriate tick function based on state. Potential states are:
   
      * DONE (does nothing)
   
      * POWERUP (calls tick_powerup())
   
      * RAMP (calls tick_ramp())
   
      * CHARGE (calls tick_charge())
   
      * default (calls stopCharge())

9. tick_powerup() checks if a battery is charging and adds to batChgCount if it is. Otherwise, it calls Bat::setCharge() for the battery. When batChgCount = num_bats, state changes to RAMP

10. tick_ramp() checks for constant voltage on the charger and sets state to CHARGE if constant voltage is reached. Otherwise, ramps current up until constant voltage is reached

11. tick_charge() checks if current has reached shutoff current or if charger measured amps are below shutoff current and calls ChargeSequence::stopCharge if current has reached shutoff levels. It also sets the charge state to DONE and calls bat::setFull() for each battery
    
      * bat::setFull() makes a rosservice call to each battery to turn charge off (BAT_CMD_CHARGE_OFF)

12. The charge is complete once the bat charge status is DONE, batteries are set to full, and the charger is off 



