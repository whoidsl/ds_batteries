/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 08/23/19.
//

#include "ds_batteries/battery.h"
#include <ds_util/ds_util.h>
#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class BatTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static bool confirm_checksum_match(std::string test_str, uint16_t expected);
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

///////////////////////////////////////////////////////////////////////////////
// BatteryModule
///////////////////////////////////////////////////////////////////////////////

TEST_F(BatTest, AcceptsGood_2)
{
  ds_batteries::Battery this_bat("0001");
  std::string testStr;
  testStr = (":0001 0001 4A3F 000C 0BDF 0000 0000 0000 0000 0064 0B8A 35B4 0005 0004 0001 0011 0010 0406 1903 00C0 0001"
             "1A90 4EBE 5813 0D6B 0D6D 0D6E 0D6E 0B90 35B1 0000 0000 0001 0018 0017 05F0 1910 00C0 0001 1A90 4EBE 5800 "
             "0D6D 0D6B 0D6C 0D6D 0B8F 35A9 FFDB FFF4 0001 0010 000F 03F6 18EE 00C0 0001 1A90 4EBE 582E 0D69 0D6A 0D6B "
             "0D6B 0B90 35AA 0008 0007 0001 0018 0016 05D6 18FA 00C0 0001 1A90 4EBE 57F9 0D6B 0D6B 0D6B 0D6A 0B8E 35A8 "
             "000C 000C 0001 0018 0017 05ED 192E 00C0 0001 1A90 4EBE 5881 0D6A 0D68 0D6A 0D6C 0B9B 35AB FFFA 0001 0001 "
             "0017 0016 05D6 1975 00C0 0001 1A90 4EBE 5839 0D6C 0D6B 0D6A 0D6A 0B8E 35A8 000C 000C 0001 0018 0017 05ED "
             "192E 00C0 0001 1A90 4EBE 5881 0D6A 0D68 0D6A 0D6C 0B9B 35AB FFFA 0001 0001 0017 0016 05D6 1975 00C0 0001 "
             "1A90 4EBE 5839 0D6C 0D6B 0D6A 0D6A 15F1\n\r");
  EXPECT_TRUE(this_bat.handleMessage(testStr));
  testStr = (":0001 0001 4A3F 000C 0577 0000 0000 0000 0000 0064 0B8D 35A9 0005 0006 0001 0011 0010 0412 1910 00C0 0001 "
             "1A90 4EBE 5813 0D69 0D6A 0D6B 0D6B 0B93 35A5 0005 0006 0001 0018 0017 05E6 1906 00C0 0001 1A90 4EBE 5800 "
             "0D6A 0D68 0D69 0D6A 0B8B 35A1 0008 0008 0001 0010 000F 03FA 18F9 00C0 0001 1A90 4EBE 582E 0D67 0D68 0D69 "
             "0D69 0B8D 35A5 0008 0008 0001 0018 0016 05D0 18EF 00C0 0001 1A90 4EBE 57F9 0D6A 0D6A 0D6A 0D68 0B8A 359D "
             "0007 0007 0001 0018 0017 05DE 1921 00C0 0001 1A90 4EBE 5881 0D67 0D65 0D67 0D69 0B9B 359D 0008 0007 0001 "
             "0017 0016 05D3 1973 00C0 0001 1A90 4EBE 5839 0D68 0D68 0D66 0D67 0B8A 359D 0007 0007 0001 0018 0017 05DE "
             "1921 00C0 0001 1A90 4EBE 5881 0D67 0D65 0D67 0D69 0B9B 359D 0008 0007 0001 0017 0016 05D3 1973 00C0 0001 "
             "1A90 4EBE 5839 0D68 0D67 0D66 0D67 0EF5\n\r");
  EXPECT_TRUE(this_bat.handleMessage(testStr));
}

TEST_F(BatTest, ParsesCounts)
{
  ds_batteries::Battery this_bat("0001");
  ASSERT_TRUE(this_bat.handleMessage(":0001 0001 4A3F 000C 0BDF 0000 0000 0000 0000 0064 0B8A 35B4 0005 0004 0001 0011 "
                                     "0010 0406 1903 00C0 0001 1A90 4EBE 5813 0D6B 0D6D 0D6E 0D6E 0B90 35B1 0000 0000 "
                                     "0001 0018 0017 05F0 1910 00C0 0001 1A90 4EBE 5800 0D6D 0D6B 0D6C 0D6D 0B8F 35A9 "
                                     "FFDB FFF4 0001 0010 000F 03F6 18EE 00C0 0001 1A90 4EBE 582E 0D69 0D6A 0D6B 0D6B "
                                     "0B90 35AA 0008 0007 0001 0018 0016 05D6 18FA 00C0 0001 1A90 4EBE 57F9 0D6B 0D6B "
                                     "0D6B 0D6A 0B8E 35A8 000C 000C 0001 0018 0017 05ED 192E 00C0 0001 1A90 4EBE 5881 "
                                     "0D6A 0D68 0D6A 0D6C 0B9B 35AB FFFA 0001 0001 0017 0016 05D6 1975 00C0 0001 1A90 "
                                     "4EBE 5839 0D6C 0D6B 0D6A 0D6A 0B8E 35A8 000C 000C 0001 0018 0017 05ED 192E 00C0 "
                                     "0001 1A90 4EBE 5881 0D6A 0D68 0D6A 0D6C 0B9B 35AB FFFA 0001 0001 0017 0016 05D6 "
                                     "1975 00C0 0001 1A90 4EBE 5839 0D6C 0D6B 0D6A 0D6A 15F1\n\r"));
  auto state = this_bat.getState();

  EXPECT_EQ(1, state.busAddress);
  EXPECT_EQ(1, state.version);
  EXPECT_EQ("20170131", state.date);
  EXPECT_EQ(0xC, state.startupCondition);
  EXPECT_EQ(0xbdf, state.uptime);
  EXPECT_EQ(0, state.overflowCount);
  EXPECT_EQ(0, state.timeoutCount);
  EXPECT_EQ(0, state.powerswitchState);
  EXPECT_EQ(0, state.chargetimeRemaining);
  EXPECT_EQ(0x64, state.chargetimeMax);

  // calculated module values
  // These need to get filled in, but it doesn't matter.  Tests don't even slightly pass.
  EXPECT_FLOAT_EQ(23.950001, state.maxPackTemp);
  EXPECT_FLOAT_EQ(22.25, state.minPackTemp);
  EXPECT_FLOAT_EQ(13.748, state.maxPackVoltage);
  EXPECT_FLOAT_EQ(13.736, state.minPackVoltage);
  EXPECT_FLOAT_EQ(3.4319999, state.minCellVoltage);
  EXPECT_FLOAT_EQ(3.438, state.maxCellVoltage);
  EXPECT_FLOAT_EQ(54.959, state.moduleVoltage);
  EXPECT_EQ(false, state.chargeEnabled);
  EXPECT_EQ(false, state.charging);
  EXPECT_EQ(false, state.discharging);
  EXPECT_FLOAT_EQ(2.5079999, state.remainingCapacity);
  EXPECT_FLOAT_EQ(12.776, state.fullCapacity);
  EXPECT_FLOAT_EQ(13.6, state.designCapacity);
  EXPECT_FLOAT_EQ(19.630556, state.percentFull);
}

bool BatTest::confirm_checksum_match(std::string test_str, uint16_t expected)
{
  std::string expected_str = ds_util::int_to_hex<uint16_t>(expected);
  std::string result_str = ds_batteries::Battery::with_checksum_text(test_str);
  int result_int = (int)strtol(result_str.substr(result_str.size() - 3).c_str(), NULL, 16);
  ROS_ERROR_STREAM("Expected: " << expected << " Got: " << result_int);
  return (expected == result_int);
  //  EXPECT_EQ((int)strtol(expected_str.c_str(), NULL, 16), (int)strtol(result.substr(result.size()-3).c_str(), NULL,
  //  16));
}

TEST_F(BatTest, CheckBadParseB1)
{
  // What I know: The checksum was taken over too long of a range. Therefoe it failed. Why did it fail this time as
  // opposed to other times?
  ROS_INFO_STREAM("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  ROS_INFO_STREAM("  Check Bad Parse B1  ");
  ROS_INFO_STREAM("-----------------------------------------------------------------------------");
  ds_batteries::Battery this_bat("0001");
  // this one works
  EXPECT_TRUE(this_bat.handleMessage(":0001 0001 4A3F 000C 0577 0000 0000 0000 0000 0064 0B8D 35A9 0005 0006 0001 0011"
                                     " 0010 0412 1910 00C0 0001 1A90 4EBE 5813 0D69 0D6A 0D6B 0D6B 0B93 35A5 0005 0006 "
                                     "0001 0018 0017 05E6 1906 00C0 0001 1A90 4EBE 5800 0D6A 0D68 0D69 0D6A 0B8B 35A1 "
                                     "0008 0008 0001 0010 000F 03FA 18F9 00C0 0001 1A90 4EBE 582E 0D67 0D68 0D69 0D69 "
                                     "0B8D 35A5 0008 0008 0001 0018 0016 05D0 18EF 00C0 0001 1A90 4EBE 57F9 0D6A 0D6A "
                                     "0D6A 0D68 0B8A 359D 0007 0007 0001 0018 0017 05DE 1921 00C0 0001 1A90 4EBE 5881 "
                                     "0D67 0D65 0D67 0D69 0B9B 359D 0008 0007 0001 0017 0016 05D3 1973 00C0 0001 1A90 "
                                     "4EBE 5839 0D68 0D68 0D66 0D67 0B8A 359D 0007 0007 0001 0018 0017 05DE 1921 00C0 "
                                     "0001 1A90 4EBE 5881 0D67 0D65 0D67 0D69 0B9B 359D 0008 0007 0001 0017 0016 05D3 "
                                     "1973 00C0 0001 1A90 4EBE 5839 0D68 0D67 0D66 0D67 0EF5\n\r"));

  // this one works
  EXPECT_TRUE(BatTest::confirm_checksum_match(
          ":0001 0001 4A3F 000C 0BDF 0000 0000 0000 0000 0064 0B8A 35B4 0005 0004 0001 0011 0010 0406 1903 00C0 0001"
          "1A90 4EBE 5813 0D6B 0D6D 0D6E 0D6E 0B90 35B1 0000 0000 0001 0018 0017 05F0 1910 00C0 0001 1A90 4EBE 5800 "
          "0D6D 0D6B 0D6C 0D6D 0B8F 35A9 FFDB FFF4 0001 0010 000F 03F6 18EE 00C0 0001 1A90 4EBE 582E 0D69 0D6A 0D6B "
          "0D6B 0B90 35AA 0008 0007 0001 0018 0016 05D6 18FA 00C0 0001 1A90 4EBE 57F9 0D6B 0D6B 0D6B 0D6A 0B8E 35A8 "
          "000C 000C 0001 0018 0017 05ED 192E 00C0 0001 1A90 4EBE 5881 0D6A 0D68 0D6A 0D6C 0B9B 35AB FFFA 0001 0001 "
          "0017 0016 05D6 1975 00C0 0001 1A90 4EBE 5839 0D6C 0D6B 0D6A 0D6A 0B8E 35A8 000C 000C 0001 0018 0017 05ED "
          "192E 00C0 0001 1A90 4EBE 5881 0D6A 0D68 0D6A 0D6C 0B9B 35AB FFFA 0001 0001 0017 0016 05D6 1975 00C0 0001 "
          "1A90 4EBE 5839 0D6C 0D6B 0D6A 0D6A 15F1\n\r", 233));
  // this does not
  EXPECT_FALSE(BatTest::confirm_checksum_match(
          ":0001 0001 4A3F 000C 0577 0000 0000 0000 0000 0064 0B8D 35A9 0005 0006 0001 0011 0010 0412 1910 00C0 0001 "
          "1A90 4EBE 5813 0D69 0D6A 0D6B 0D6B 0B93 35A5 0005 0006 0001 0018 0017 05E6 1906 00C0 0001 1A90 4EBE 5800 "
          "0D6A 0D68 0D69 0D6A 0B8B 35A1 0008 0008 0001 0010 000F 03FA 18F9 00C0 0001 1A90 4EBE 582E 0D67 0D68 0D69 "
          "0D69 0B8D 35A5 0008 0008 0001 0018 0016 05D0 18EF 00C0 0001 1A90 4EBE 57F9 0D6A 0D6A 0D6A 0D68 0B8A 359D "
          "0007 0007 0001 0018 0017 05DE 1921 00C0 0001 1A90 4EBE 5881 0D67 0D65 0D67 0D69 0B9B 359D 0008 0007 0001 "
          "0017 0016 05D3 1973 00C0 0001 1A90 4EBE 5839 0D68 0D68 0D66 0D67 0B8A 359D 0007 0007 0001 0018 0017 05DE "
          "1921 00C0 0001 1A90 4EBE 5881 0D67 0D65 0D67 0D69 0B9B 359D 0008 0007 0001 0017 0016 05D3 1973 00C0 0001 "
          "1A90 4EBE 5839 0D68 0D67 0D66 0D67 000B\n\r", 255));
}

TEST_F(BatTest, CommandWithChecksum)
{
  ds_batteries::Battery this_bat;

  std::string test_str = "00010000";
  std::string expected = ds_util::int_to_hex<uint16_t>(000000001);
  std::string result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ(test_str + "00" + expected +"\r", result);
  // ROS_ERROR_STREAM("kitchen sink: " << result << " ends with: " << expected);

}

extern int fix_sign_16(int in);

TEST(BatSignTest, basic) {
  EXPECT_EQ(0, fix_sign_16(0));
  EXPECT_EQ(-1, fix_sign_16(0xffff));
  EXPECT_EQ(-2, fix_sign_16(0xfffe));
  EXPECT_EQ(std::numeric_limits<int16_t>::min(), fix_sign_16(0x8000));
  EXPECT_EQ(std::numeric_limits<int16_t>::max(),
      fix_sign_16(std::numeric_limits<int16_t>::max()));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
