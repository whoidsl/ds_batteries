/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 08/19/19.
//

#include "ds_batteries/battery.h"
#include <ds_core_msgs/IoCommandList.h>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <limits>

using namespace ds_batteries;

const uint16_t SWITCHSTATE_DISCHARGE = 0x01;
const uint16_t SWITCHSTATE_CHARGE_ENABLE = 0x02;
const uint16_t SWITCHSTATE_CHARGE = 0x04;

const uint16_t PACKSTATUS_FULLY_DISCHARGED = 0x0010;
const uint16_t PACKSTATUS_FULLY_CHARGED = 0x0020;
const uint16_t PACKSTATUS_DISCHARGING = 0x0040;
const uint16_t PACKSTATUS_INITIALIZED = 0x0080;
const uint16_t PACKSTATUS_REMAINING_TIME_ALARM = 0x0100;
const uint16_t PACKSTATUS_REMAINING_CAPACITY_ALARM = 0x0200;
const uint16_t PACKSTATUS_TERMINATE_DISCHARGE_ALARM = 0x0800;
const uint16_t PACKSTATUS_OVER_TEMP_ALARM = 0x1000;
const uint16_t PACKSTATUS_TERMINATE_CHARGE_ALARM = 0x4000;
const uint16_t PACKSTATUS_OVER_CHARGED_ALARM = 0x8000;

std::string parseSbdsDate(uint16_t date) {
    unsigned int day = static_cast<unsigned int>(date & 0x001f);
    unsigned int month = static_cast<unsigned int>((date & 0x01e0) >> 5);
    unsigned int year = static_cast<unsigned int>((date & 0xfe00) >> 9) + 1980;

    std::stringstream ret;
    ret <<std::setfill('0') <<std::setw(4) <<year;
    ret <<std::setfill('0') <<std::setw(2) <<month;
    ret <<std::setfill('0') <<std::setw(2) <<day;
    return ret.str();
}

Battery::Battery() : ds_base::DsBusDevice()
{
  //address_ = "0002";
  state = ds_hotel_msgs::ClioBattery{};
  bat = sensor_msgs::BatteryState{};
  charge_requested = false; 
}

Battery::Battery(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  // do nothing
  //address_ = "0002";
  state = ds_hotel_msgs::ClioBattery{};
  bat = sensor_msgs::BatteryState{};
  charge_requested = false; 
}

Battery::Battery(std::string address) : ds_base::DsBusDevice()
{
  address_ = address;
  state = ds_hotel_msgs::ClioBattery{};
  bat = sensor_msgs::BatteryState{};
  charge_requested = false; 
}

Battery::~Battery() = default;

const ds_hotel_msgs::ClioBattery& Battery::getState() const {
  return state;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#define NUM_PACKS 8
#define NUM_VOLTAGES 8 //TODO: Changed from 14
#define NUM_TEMPERATURES 8 //TODO: Changed from 56

int fix_sign_16(int in) {
  // the batteries dump signed numbers as hex codes.  Without a - sign,
  // stoi will always return a positive number.  This function simply does the
  // conversion to signed
  return *(reinterpret_cast<int16_t*>(&in));
}

bool Battery::handleMessage(const std::string& msg)
{
  std::stringstream ss(msg);
  std::string token;
  char chars[] ="\xff?:";
  std::vector <std::string> tokens;
  while (ss >> token)
  {
    tokens.push_back(token);
  }
  std::string busAddress = tokens[0];
  for (int i =0; i<strlen(chars);++i) {
    busAddress.erase(std::remove(busAddress.begin(), busAddress.end(), chars[i]), busAddress.end());
  }
  if (address_ != busAddress) {
      // this is for another battery.  Just skip it.
    //ROS_ERROR_STREAM("Address not equivalent to bus Address");
    return false;
  }
  if (tokens.size() < 18*NUM_PACKS+10) {
    ROS_ERROR_STREAM("Battery message too short (" <<tokens.size() <<" < " <<18*NUM_PACKS+27 <<", skipping...");
    return false;
  }
  //ROS_INFO_STREAM("Bus Address: "<<busAddress<<" and batt address: "<<address_);
  try {
    state.busAddress = std::stoi(busAddress, nullptr, 16);
    state.version = std::stoi(tokens[1], nullptr, 16);
    uint16_t raw_date = std::stoi(tokens[2], nullptr, 16);
    state.date = parseSbdsDate(raw_date);
    state.startupCondition = std::stoi(tokens[3], nullptr, 16);
    if (state.startupCondition != 12) {
      ROS_ERROR_STREAM("BAD MICROCHIP STARTUP CODE");
    }
    state.uptime = std::stoi(tokens[4], nullptr, 16);
    state.overflowCount = std::stoi(tokens[5], nullptr, 16);
    state.timeoutCount = std::stoi(tokens[6], nullptr, 16);
    state.powerswitchState = std::stoi(tokens[7],nullptr,16);
    state.chargetimeRemaining = std::stoi(tokens[8], nullptr, 16);
    state.chargetimeMax = std::stoi(tokens[9], nullptr, 16);


    state.packs.resize(NUM_PACKS);
    bat.cell_voltage.resize(NUM_VOLTAGES);

    state.remainingCapacity = 0;
    state.fullCapacity = 0;
    state.designCapacity = 0;
    state.minPackTemp    =  std::numeric_limits<float>::infinity();
    state.maxPackTemp    = -std::numeric_limits<float>::infinity();
    state.minPackVoltage =  std::numeric_limits<float>::infinity();
    state.maxPackVoltage = -std::numeric_limits<float>::infinity();
    state.minCellVoltage =  std::numeric_limits<float>::infinity();
    state.maxCellVoltage = -std::numeric_limits<float>::infinity();
    state.moduleVoltage = 0;
    state.moduleStatus = 0;
    double voltage_accum = 0;
    float capacity_design_accum = 0;
    float capacity_full_accum = 0;
    float capacity_remaining_accum = 0;

    state.remainingCapacity = std::numeric_limits<float>::infinity();
    state.fullCapacity = std::numeric_limits<float>::infinity();
    state.designCapacity = std::numeric_limits<float>::infinity();

    for (size_t i=0;i<NUM_PACKS;i++) {
      // parse the pack
      state.packs[i].temperature = (std::stoi(tokens[18*i+10], nullptr, 16) * .10) - 273.15; //.10 deg K to C
      state.packs[i].voltage = std::stoi(tokens[18*i+11], nullptr, 16) * 0.001; //mV to V
      state.packs[i].current = fix_sign_16(std::stoi(tokens[18*i+12], nullptr, 16)) * 0.001; // mA to A
      state.packs[i].avgCurrent = fix_sign_16(std::stoi(tokens[18*i+13], nullptr, 16)) * 0.001; // mA to A
      state.packs[i].maxError = std::stoi(tokens[18*i+14], nullptr, 16);
      state.packs[i].relativeSOC = std::stoi(tokens[18*i+15], nullptr, 16);
      state.packs[i].absoluteSOC = std::stoi(tokens[18*i+16], nullptr, 16);
      state.packs[i].remainingCapacity = std::stoi(tokens[18*i+17], nullptr, 16) * 0.001; // mAh to Ah
      state.packs[i].fullCapacity = std::stoi(tokens[18*i+18], nullptr, 16) * 0.001; // mAh to Ah
      state.packs[i].status = std::stoi(tokens[18*i+19], nullptr, 16);
      state.packs[i].countCycle = std::stoi(tokens[18*i+20], nullptr, 16);
      state.packs[i].capacityDesign = std::stoi(tokens[18*i+21], nullptr, 16) * 0.001; // mAh to Ah
      uint16_t raw_date = std::stoi(tokens[18*i+22], nullptr, 16);
      state.packs[i].dateManufacture = parseSbdsDate(raw_date);
      state.packs[i].serialNumber = std::stoi(tokens[18*i+23], nullptr, 16);
      // each pack as 4 (pairs) of cells in a string
      state.packs[i].highCellVoltage = std::stoi(tokens[18*i+24], nullptr, 16) * 0.001; // mV to V
      state.packs[i].mid1CellVoltage = std::stoi(tokens[18*i+25], nullptr, 16) * 0.001; // mV to V
      state.packs[i].mid2CellVoltage = std::stoi(tokens[18*i+26], nullptr, 16) * 0.001; // mV to V
      state.packs[i].lowCellVoltage = std::stoi(tokens[18*i+27], nullptr, 16) * 0.001; // mV to V

      // update our global estimates
      float min_cell = std::min(
                                std::min(state.packs[i].lowCellVoltage, state.packs[i].mid1CellVoltage),
                                std::min(state.packs[i].mid2CellVoltage, state.packs[i].highCellVoltage));
      float max_cell = std::max(
                                std::max(state.packs[i].lowCellVoltage, state.packs[i].mid1CellVoltage),
                                std::max(state.packs[i].mid2CellVoltage, state.packs[i].highCellVoltage));

      state.minPackTemp = std::min(state.minPackTemp, state.packs[i].temperature);
      state.maxPackTemp = std::max(state.maxPackTemp, state.packs[i].temperature);
      state.minPackVoltage = std::min(state.minPackVoltage, state.packs[i].voltage);
      state.maxPackVoltage = std::max(state.maxPackVoltage, state.packs[i].voltage);
      state.minCellVoltage = std::min(state.minCellVoltage, min_cell);
      state.maxCellVoltage = std::max(state.maxCellVoltage, max_cell);

      // For error tracking, we'll bitwise OR all the status fields
      state.moduleStatus |= state.packs[i].status;

      bat.cell_voltage[i] = state.packs[i].voltage;

      // voltage is tricky; consecutive pack pairs are in parallel, so average and then sum them
      if (i % 2 == 0) {
          // first pack in pair
          voltage_accum = 0.5 * state.packs[i].voltage;
          capacity_design_accum = state.packs[i].capacityDesign;
          capacity_full_accum = state.packs[i].fullCapacity;
          capacity_remaining_accum = state.packs[i].remainingCapacity;
      } else {
          voltage_accum += 0.5 * state.packs[i].voltage;
          state.moduleVoltage += voltage_accum;
          capacity_design_accum += state.packs[i].capacityDesign;
          capacity_full_accum += state.packs[i].fullCapacity;
          capacity_remaining_accum += state.packs[i].remainingCapacity;

          state.remainingCapacity = std::min(state.remainingCapacity, capacity_remaining_accum);
          state.fullCapacity = std::min(state.fullCapacity, capacity_full_accum);
          state.designCapacity = std::min(state.designCapacity, capacity_design_accum);
      }
    }
    bat.voltage = state.moduleVoltage;

  }
    catch (...) {

        ROS_ERROR_STREAM("COULD NOT PARSE BAT STATUS MSG for bat: " << state.busAddress);
	return false;
    }

    state.discharging = ((state.powerswitchState & SWITCHSTATE_DISCHARGE) == SWITCHSTATE_DISCHARGE);
    state.chargeEnabled = ((state.powerswitchState & SWITCHSTATE_CHARGE_ENABLE) == SWITCHSTATE_CHARGE_ENABLE);
    state.charging = ((state.powerswitchState & SWITCHSTATE_CHARGE) == SWITCHSTATE_CHARGE);
    
    if (state.charging) {
        bat.power_supply_status = sensor_msgs::BatteryState::POWER_SUPPLY_STATUS_CHARGING;
    } else if (state.discharging) {
        bat.power_supply_status = sensor_msgs::BatteryState::POWER_SUPPLY_STATUS_DISCHARGING;
    } else {
        bat.power_supply_status = sensor_msgs::BatteryState::POWER_SUPPLY_STATUS_NOT_CHARGING;
    }

    // update capacity
    state.percentFull = state.remainingCapacity / state.fullCapacity * 100.0;

    updateStatus();

    return true;
}

void Battery::updateStatus() {
    if (state.moduleStatus & PACKSTATUS_INITIALIZED) {
        ROS_INFO_STREAM("Battery freshly initialized!");
    }
    if (state.moduleStatus & PACKSTATUS_REMAINING_TIME_ALARM) {
        ROS_ERROR_STREAM("Remaining time alarm on battery module: " <<state.busAddress);
    }
    if (state.moduleStatus & PACKSTATUS_REMAINING_CAPACITY_ALARM) {
        ROS_ERROR_STREAM("Remaining capacity alarm on battery module: " <<state.busAddress);
    }
    if (state.moduleStatus & PACKSTATUS_TERMINATE_DISCHARGE_ALARM) {
        ROS_ERROR_STREAM("Terminate discharge alarm on battery module: " <<state.busAddress);
    }
    if (state.moduleStatus & PACKSTATUS_OVER_TEMP_ALARM) {
        ROS_ERROR_STREAM("Over temperature alarm on battery module: " <<state.busAddress);
    }
    if (state.moduleStatus & PACKSTATUS_TERMINATE_CHARGE_ALARM) {
        ROS_ERROR_STREAM("Terminate charge alarm on battery module: " <<state.busAddress);
    }
    if (state.moduleStatus & PACKSTATUS_OVER_CHARGED_ALARM) {
        ROS_ERROR_STREAM("Over charged alarm on battery module: " <<state.busAddress);
    }

    // Updated sensor_msgs/BatteryState message
    bat.design_capacity = state.designCapacity;
    bat.percentage = state.percentFull;
    bat.present = true;
    bat.location = std::to_string(state.busAddress);
}
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx

//------------------------------------------------------------
// Private inner stuff
bool Battery::_bat_cmd(const ds_hotel_msgs::BatteryCmd::Request& req, const ds_hotel_msgs::BatteryCmd::Response& resp)
{
  std::string cmdstr = address_+"";

  switch (req.command)
  {
      case ds_hotel_msgs::BatteryCmd::Request::BAT_CMD_BOTH_OFF:
          charge_requested = false;
          cmdstr += "00030000";
          ROS_INFO_STREAM("Sending off command");
          break;
      case ds_hotel_msgs::BatteryCmd::Request::BAT_CMD_DISCHARGE_ON:
          cmdstr += "00010001";
          ROS_INFO_STREAM("Sending discharge command");
          break;
      case ds_hotel_msgs::BatteryCmd::Request::BAT_CMD_DISCHARGE_OFF:
          cmdstr += "00010000";
          ROS_INFO_STREAM("Sending discharge off command");
          break;
      case ds_hotel_msgs::BatteryCmd::Request::BAT_CMD_CHARGE_ON:
          charge_requested = true;
          cmdstr += "00020002";
          ROS_INFO_STREAM("Sending charge command");
          break;
      case ds_hotel_msgs::BatteryCmd::Request::BAT_CMD_CHARGE_OFF:
          charge_requested = false;
          cmdstr += "00020000";
          ROS_INFO_STREAM("Sending charge off command");
          break;
      case ds_hotel_msgs::BatteryCmd::Request::BAT_CMD_BOTH_ON:
          charge_requested = true;
          cmdstr += "00030003";
          ROS_INFO_STREAM("Sending both chg/discharge on command");
          break;
      default:
          return false;
  }

  _send_preempt(cmdstr);
  return true;
}

void Battery::_setup_charge_cmd(const ros::TimerEvent&) {
  //ROS_ERROR_STREAM("Called setup charge");
   if (charge_requested == true && state.percentFull < 100) {
	_send_charge_cmd();
}
    else {
	ROS_WARN_STREAM_ONCE("Callback not sending charge command");
}
}

void Battery::_send_charge_cmd() {
    // The batteries need to receive the charge command before the timeout in order to continue
    // charging. So we send the command when the timeout is below 20 but not 0.
    
    ds_core_msgs::IoSMcommand sm_cmd;
    sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_PREEMPT;

    ds_core_msgs::IoCommand cmd;
    cmd.delayBefore_ms = 100;
    cmd.timeout_ms = 10000;
    cmd.delayAfter_ms = 100;
    cmd.forceNext = false;
    cmd.timeoutWarn = true;
    cmd.emitOnMatch = true;

    // query state status
    std::string cmdstr = address_+"";
    cmdstr += "00020002";
    ROS_ERROR_STREAM("Resending charge command");
    cmd.command = with_checksum_text(cmdstr);
    sm_cmd.request.commands.push_back(cmd);

    if (IosmCmd().waitForExistence(ros::Duration(1.0)))
    {
        if (IosmCmd().call(sm_cmd))
        {
        }
        else
        {
        }
    }
    else
    {
    }
}

void Battery::_setup_preempt(const std::string& cmdstr)
{
  std::string bat_cmdstr = address_ + cmdstr;
  _send_preempt(bat_cmdstr);
}
void Battery::_send_preempt(const std::string& cmdstr)
{
  ds_core_msgs::IoCommandList cmdList;
  ds_core_msgs::IoCommand cmd;

  cmd.delayBefore_ms = 100;
  cmd.timeout_ms = 10000;
  cmd.delayAfter_ms = 100;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  cmd.command = with_checksum_text(cmdstr);
  cmdList.cmds.push_back(cmd);

  PreemptCmd().publish(cmdList);
}

void Battery::_send_wakeup_cmd()
{
    // We want to wait to ensure bus node starts up first before we start polling batteries
    // so we add a waitForExistence with a specified duration when setting up the IO state machine

    ds_core_msgs::IoSMcommand sm_cmd;
    sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_PREEMPT;

    ds_core_msgs::IoCommand cmd;
    cmd.delayBefore_ms = 100;
    cmd.timeout_ms = 10000;
    cmd.delayAfter_ms = 100;
    cmd.forceNext = false;
    cmd.timeoutWarn = true;
    cmd.emitOnMatch = true;

    // query state status
    std::string command = address_ + "00000000";
    cmd.command = with_checksum_text(command);
    sm_cmd.request.commands.push_back(cmd);

    if (IosmCmd().waitForExistence(ros::Duration(5.0)))
    {
        if (IosmCmd().call(sm_cmd))
        {
        }
        else
        {
        }
    }
    else
    {
    }
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
void Battery::setupIoSM() {
    ds_base::DsBusDevice::setupIoSM();

    _send_wakeup_cmd();

    // Prepare our I/O state machine command
    ds_core_msgs::IoSMcommand sm_cmd;
    sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

    ds_core_msgs::IoCommand cmd;
    cmd.delayBefore_ms = 1000;
    cmd.timeout_ms = 10000;
    cmd.delayAfter_ms = 1000;
    cmd.forceNext = false;
    cmd.timeoutWarn = true;
    cmd.emitOnMatch = true;

    // query state status
    std::string command = address_ + "00000000";
    cmd.command = with_checksum_text(command);
    sm_cmd.request.commands.push_back(cmd);
        if (IosmCmd().call(sm_cmd)) {
        } else {
        }
    }

void Battery::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();
  ROS_INFO_STREAM("setting up params");
  address_ = ros::param::param<std::string>("~sail_address", "0003");
  ROS_INFO_STREAM("Reading battery from "<<ros::names::resolve("~sail_address"));
  ROS_INFO_STREAM("Address is: " <<address_);

}
void Battery::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();
  // prepare our publishers
  auto nh = nodeHandle();
  bat_state_pub_ = nh.advertise<ds_hotel_msgs::ClioBattery>(ros::this_node::getName() + "/state", 10);
  bat_pub_ = nh.advertise<sensor_msgs::BatteryState>("bat", 10);
  
  // Prepare a timer
  chg_timer = nh.createTimer(ros::Duration(20.0), &Battery::_setup_charge_cmd, this);
  //ROS_ERROR_STREAM("Created timer");

  // prepare to a service for indexing, sleeping, etc
  bat_ctrl_cmd_ = nh.advertiseService<ds_hotel_msgs::BatteryCmd::Request, ds_hotel_msgs::BatteryCmd::Response>(
      ros::this_node::getName() + "/cmd", boost::bind(&Battery::_bat_cmd, this, _1, _2));
  ROS_INFO_STREAM("connections set");
}

///*-----------------------------------------------------------------------------*///
///*   TOP FUNCTIONS: receive incoming raw data, create messages, and publish    *///
///*-----------------------------------------------------------------------------*///
void Battery::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  ROS_INFO_STREAM("Messsage size: " <<bytes.data.size());
  std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());
  if (handleMessage(msg))
  {
    // fill in header timestamps from I/O time
    state.header.stamp = bytes.ds_header.io_time;
    state.ds_header.io_time = bytes.ds_header.io_time;

    // publish!
    bat.header = state.header;

    // publish battery states
    bat_pub_.publish(bat);
    bat_state_pub_.publish(state);
  }
  else
  {
      ROS_INFO_STREAM("Unable to handle battery message");
  }
}

std::string Battery::with_checksum_text(std::string cmd)
{

  int accum = 0;
  const char* cmd_char = cmd.c_str();

  for (int i = 0; i < strlen(cmd_char); i++)
  {
      if (cmd_char[i] >=48 && cmd_char[i]<=57) {
          accum+=(int)cmd_char[i] - 48;
      }
  }
  std::string withchksum_cmd = cmd + "00" + ds_util::int_to_hex<uint16_t>(accum) + "\r";
  return withchksum_cmd;
}
