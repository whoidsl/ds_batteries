/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 8/22/19.
//
#include <ds_hotel_msgs/BatteryCmd.h>
#include <ds_hotel_msgs/PowerSupplyCommand.h>
#include "ds_batteries/batsupervisor.h"
#include "ds_batteries/battery.h"

using namespace ds_batteries;

BatSupervisor::BatSupervisor() : DsProcess()
{
}

BatSupervisor::BatSupervisor(int argc, char** argv, const std::string& name) : DsProcess(argc, argv, name)
{
}

BatSupervisor::~BatSupervisor()
{
    safeBats();
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void BatSupervisor::startCharge()
{
    stopCharge();
    ROS_INFO_STREAM("CHARGE STARTED");
    for (size_t i=0;i<chgs.size();i++) {
        manager = new ChargeSequence(config, bats, chgs[i], multi_timer);
    }
    for (size_t i=0;i<bats.size();i++) {
        ROS_ERROR_STREAM("Chg sequence for : "<< bats[i]);
    }
}

void BatSupervisor::stopCharge()
{
    ROS_INFO_STREAM("CHARGE STOPPED");
    batsOff();
    for (size_t i=0;i<chgs.size();i++)
    {
        chgs[i]->setOff();
    }
    for (int i = 0; i < bats.size(); i++)
    {
        bats[i]->setOff();
    }
    
}

void BatSupervisor::batsOn()
{
    ROS_INFO_STREAM("BATS ON");
    for (int i = 0; i < bats.size(); i++)
    {
        bats[i]->setOn();
    }
}

void BatSupervisor::batsOff()
{
    ROS_INFO_STREAM("BATS OFF");
    for (int i = 0; i < bats.size(); i++)
    {
        bats[i]->setOff();
    }
}

void BatSupervisor::shoreOn()
{
    ROS_INFO_STREAM("SHORE ON");
    shore->setVC(61, 8.0);  // TODO: Verify these numbers
}

void BatSupervisor::shoreOff()
{
    ROS_INFO_STREAM("SHORE OFF");
    shore->setOff();
}
void BatSupervisor::safeBats()
{
    ROS_INFO_STREAM("SAFE BATS");
    stopCharge();
}

void BatSupervisor::pubLatest(const ros::TimerEvent&)
{
    auto now = ds_hotel_msgs::BatMan{};

    // Add timestamp.
    now.header.stamp = ros::Time::now();
    now.ds_header.io_time = now.header.stamp;

    now.moduleVolt.resize(num_bats);
    now.moduleAh.resize(num_bats);
    now.moduleCapacity.resize(num_bats);
    now.modulePercent.resize(num_bats);
    now.moduleDischarging.resize(num_bats);
    now.moduleCharging.resize(num_bats);
    now.num_bats = 0;
    now.maxModuleVolt = 0;
    now.minModuleVolt = 100;

    now.maxModuleVolt = -std::numeric_limits<float>::infinity();
    now.minModuleVolt = std::numeric_limits<float>::infinity();
    now.maxCellVolt = -std::numeric_limits<float>::infinity();
    now.minCellVolt = std::numeric_limits<float>::infinity();
    now.maxCellTemp = -std::numeric_limits<float>::infinity();
    now.minCellTemp = std::numeric_limits<float>::infinity();

    for (int i = 0; i < num_bats; i++)
    {
        auto bat = bats[i]->get_latest();
        now.moduleVolt[i] = bat.moduleVoltage;
        now.moduleAh[i] = bat.remainingCapacity;
        now.moduleCapacity[i] = bat.fullCapacity;
        now.modulePercent[i] = bat.percentFull;
        now.moduleDischarging[i] = bat.discharging;
        now.moduleCharging[i] = bat.charging;

        if (bat.busAddress == 0)
        {
            // Battery hasn't published yet, so skip to the next battery
            continue;
        }
        now.num_bats += 1;
        // switch temp not implemented for these batteries
        now.minSwitchTemp = 0;
        now.maxSwitchTemp = 0;
        now.maxModuleVolt = std::max(now.maxModuleVolt, bat.moduleVoltage);
        now.minModuleVolt = std::min(now.minModuleVolt, bat.moduleVoltage);
        now.maxCellVolt = std::max(now.maxCellVolt, bat.maxCellVoltage);
        now.minCellVolt = std::min(now.minCellVolt, bat.minCellVoltage);
        now.maxCellTemp = std::max(now.maxCellTemp, bat.maxPackTemp);
        now.minCellTemp = std::min(now.minCellTemp, bat.minPackTemp);

        //Do some handling based on bat status
        if (bat.percentFull > -10 && bat.percentFull < 110)
        {
            if (bat.discharging) {
                now.capacityAh += bat.fullCapacity;
                now.chargeAh += bat.remainingCapacity;
            }
        }
        else
        {
            ROS_ERROR_STREAM("Bat " << bat.busAddress << " has an out-of-range percent");
        }
    }
    if (now.capacityAh < 0.01) {
        now.percentFull = 0;
    } else {
        now.percentFull = 100.0 * now.chargeAh / now.capacityAh;
    }

    now.capacityCoulombs = now.capacityAh * 3600.0;
    now.chargeCoulombs = now.chargeAh * 3600.0;
    latest = now;
    batsupervisor_pub_.publish(latest);
}

void BatSupervisor::stop()
{
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void BatSupervisor::setupParameters()
{
    num_bats = 4;
    num_chgs = 1;

    auto batsupervisor_prefix = ros::this_node::getNamespace() + "/";
    auto bat_prefix = ros::param::param<std::string>("~bat_path", batsupervisor_prefix);
    bats.resize(num_bats);
    for (int i = 0; i < num_bats; i++)
    {
        bats[i] = new Bat(bat_prefix + "bat" + std::to_string(i + 1), i + 1);
        ROS_ERROR_STREAM("BAT: "<<(bat_prefix+"bat" + std::to_string(i+1),i+1));
    }

    auto chg_prefix = ros::param::param<std::string>("~chg_path", batsupervisor_prefix);
    chgs.resize(num_chgs);
    for (int i = 0; i < num_chgs; i++)
    {
        chgs[i] = new Pwr(chg_prefix + "chg" + std::to_string(i + 1), i + 1);
        ROS_ERROR_STREAM("CHG: "<< (chg_prefix + "chg" + std::to_string(i+1),i+1));

    }
    auto shore_addr = ros::param::param<std::string>("~shore_address", batsupervisor_prefix + "shore");
    shore  = new Pwr(shore_addr,0);

}

void BatSupervisor::setupConnections()
{
    bat_clients_.resize(num_bats);
    bat_subs_.resize(num_bats);
    chg_clients_.resize(num_chgs);
    chg_subs_.resize(num_chgs);

    auto nh = nodeHandle();
    for (int i = 0; i < num_bats; i++)
    {
//    ROS_INFO_STREAM(bats[i]->get_addr());
        bat_clients_[i] = nh.serviceClient<ds_hotel_msgs::BatteryCmd>(bats[i]->get_addr() + "/cmd");
        bat_subs_[i] = nh.subscribe(bats[i]->get_addr() + "/state", 1, &Bat::updateLatest, bats[i]);
        bats[i]->set_client(bat_clients_[i]);
        if (i < num_chgs)
        {
            chg_clients_[i] = nh.serviceClient<ds_hotel_msgs::PowerSupplyCommand>(chgs[i]->get_addr() + "/cmd");
            chg_subs_[i] = nh.subscribe(chgs[i]->get_addr() + "/dcpwr", 1, &Pwr::updateLatest, chgs[i]);
            chgs[i]->set_client(chg_clients_[i]);
        }
    }

    shore_client_ = nh.serviceClient<ds_hotel_msgs::PowerSupplyCommand>(shore->get_addr() + "/cmd");
    shore_sub_ = nh.subscribe(shore->get_addr() + "/dcpwr", 1, &Pwr::updateLatest, shore);
    shore->set_client(shore_client_);

    chg_cmd_ = nh.advertiseService<ds_hotel_msgs::ClioChgCmd::Request, ds_hotel_msgs::ClioChgCmd::Response>(
            ros::this_node::getName() + "/chg_cmd", boost::bind(&BatSupervisor::_chg_cmd, this, _1, _2));

    bat_pwr_cmd_ = nh.advertiseService<ds_hotel_msgs::PowerCmd::Request, ds_hotel_msgs::PowerCmd::Response>(
            ros::this_node::getName() + "/bat_pwr_cmd", boost::bind(&BatSupervisor::_bat_pwr_cmd, this, _1, _2));

    shore_pwr_cmd_ = nh.advertiseService<ds_hotel_msgs::PowerCmd::Request, ds_hotel_msgs::PowerCmd::Response>(
        ros::this_node::getName() + "/shore_pwr_cmd", boost::bind(&BatSupervisor::_shore_pwr_cmd, this, _1, _2));

    batsupervisor_pub_ = nh.advertise<ds_hotel_msgs::BatMan>(ros::this_node::getName() + "/state", 10);

    multi_timer = nh.createTimer(ros::Duration(1), &BatSupervisor::tick, this);
    multi_timer.stop();

    pub_timer = nh.createTimer(ros::Duration(3), &BatSupervisor::pubLatest, this);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool BatSupervisor::_chg_cmd(const ds_hotel_msgs::ClioChgCmd::Request& req, const ds_hotel_msgs::ClioChgCmd::Response& resp)
{
    switch (req.command)
    {
        case ds_hotel_msgs::ClioChgCmd::Request::CHARGE_CMD_OFF:
	        stopCharge();
	        ROS_INFO_STREAM("Sending charge off command");
            break;
        case ds_hotel_msgs::ClioChgCmd::Request::CHARGE_CMD_CHARGE:
	        startCharge();
	        ROS_ERROR_STREAM("Sending Charge on command");
            break;
        default:
            return false;
    }
    return true;
}

bool BatSupervisor::_bat_pwr_cmd(const ds_hotel_msgs::PowerCmd::Request& req, const ds_hotel_msgs::PowerCmd::Response& resp)
{
    switch (req.command)
    {
        case ds_hotel_msgs::PowerCmd::Request::POWER_CMD_OFF:
            batsOff();
            break;
        case ds_hotel_msgs::PowerCmd::Request::POWER_CMD_ON:
            batsOn();
            break;
        default:
            return false;
    }
    return true;
}

bool BatSupervisor::_shore_pwr_cmd(const ds_hotel_msgs::PowerCmd::Request& req, const ds_hotel_msgs::PowerCmd::Response& resp) {
    switch (req.command)
    {
        case ds_hotel_msgs::PowerCmd::Request::POWER_CMD_OFF:
            shoreOff();
            break;
        case ds_hotel_msgs::PowerCmd::Request::POWER_CMD_ON:
            shoreOn();
            break;
        default:
            return false;
    }
    return true;
}

void BatSupervisor::tick(const ros::TimerEvent&) {
    if (manager != nullptr) {
        manager->tick();
    }
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// ChargeSequence
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ChargeSequence::ChargeSequence(const ChargeConfig& _c, std::vector<Bat*> _batteries, Pwr* _charger, ros::Timer _timer)
: config(_c), batteries(_batteries), charger(_charger), timer(_timer)
{
    ROS_WARN_STREAM_ONCE("Created new chargesequence!");
    batChgCount = 0;
    num_bats = batteries.size();
    starting = false;
    timer.setPeriod(ros::Duration(30));
    timer.start();
    
    batteryCommsTimeout = config.getBatteryTimeout();
    chargerCommsTimeout = config.getChargerTimeout();

    current = config.getINITIAL_C();
    voltage = config.getTARGET_V();
    //voltage = 66.5;
    ROS_WARN_STREAM("Voltage: "<<voltage<<" and current: "<<current);
    terminalVmax = config.getMAX_V();
    //terminalVmax = 68.5;

    state = ChargeState::POWERUP;
    ROS_WARN_STREAM_ONCE("CALLING START CHARGE WITH CURR: "<<current<<" |VOLTAGE: "<<voltage<<" |Vmax: "<<terminalVmax);
    startCharge();
}

ChargeSequence::~ChargeSequence() {
    stopCharge();
}

void ChargeSequence::tick()
{
    if (charge_notOk()) {
        ROS_ERROR_STREAM("Battery is not OK, aborting");
        stopCharge();
        return;
    }
    
    for (size_t i=0;i<num_bats;i++) {
        batteries[i]->setCharge();
	ROS_WARN_STREAM("Tick set charge");
    }

    switch (state)
    {
        case DONE:
        //do nothing
	    ROS_WARN_STREAM("BAT CHG STATE: DONE");
            break;
        case POWERUP:
        //bring up all the batteries, ensure chg is good
        ROS_WARN_STREAM("BAT CHG STATE: POWERUP");
            tick_powerup();
            break;
        case RAMP:
        //Ramp up to full charge state
	    ROS_WARN_STREAM("BAT CHG STATE: RAMP");
            tick_ramp();
            break;
        case CHARGE:
        //Normal charge state
	    ROS_WARN_STREAM("BAT CHG STATE: CHARGE");
            tick_charge();
            break;
        default:
            ROS_ERROR_STREAM("Battery charge sequence has an unknown state, aborting charge");
            stopCharge();
    }
}

void ChargeSequence::tick_powerup() {
    for (size_t i=0;i<num_bats;i++) {
	if (batteries[i]->get_is_charging()) {
        	ROS_WARN_STREAM("BAT "<<batteries[i]<<" is charging");
        	batChgCount++;
    	}
    	else {
		ROS_ERROR_STREAM("BAT "<<batteries[i]<<" not set to charge, resending");
		batteries[i]->setCharge();
		ROS_WARN_STREAM("Tick powerup set charge");
    	}
    }
    if (batChgCount == num_bats) {
        ROS_WARN_STREAM("All bats charging, proceeding to ramp state");
        state = ChargeState::RAMP;
        charger->setVC(voltage, current);
	ROS_WARN_STREAM("powerup setvc: "<<voltage<<" and current: "<<current);
        return;
    }
}

void ChargeSequence::tick_ramp() {
    if (charger->get_is_const_voltage()) {
        ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " -->" <<
        " reached constant voltage during ramp; proceeding directly to charge");
        voltage = terminalVmax + current * config.getCABLE_R();
        ROS_WARN_STREAM("tick_ramp voltage: "<<voltage<<" and current: "<<current);
	state = ChargeState::CHARGE;
        charger->setVC(voltage, current);
        return;
    }

    if (current + config.getRAMP_INCR_C() < config.getMAX_C()) {
        current += config.getRAMP_INCR_C();
    }
    else {
        current = config.getMAX_C();
        state = ChargeState::CHARGE;

        ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << " has finished RAMP V="<<voltage<<"V C="<< current << "A");
    }
    voltage = terminalVmax + current * config.getCABLE_R();
    charger->setVC(voltage, current);
    ROS_WARN_STREAM("ramp setvc: "<<voltage<<" and current: "<<current);
    }

void ChargeSequence::tick_charge() {
   // Also use measured current-- if there's no cable resistance,
  // Daniel's current-pulsing method won't work.  So fall back to standard CC/CV charging
  if (current <= config.getSHUTOFF_C() || charger->get_meas_amps() < config.getSHUTOFF_C())
  {
    // Current has trickled off
    state = ChargeState::DONE;

    ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << " has finished charging, turning off charge");

    stopCharge();
    for (size_t i=0;i<num_bats;i++) {
        batteries[i]->setFull();
    }
    return;
  }

  if (charger->get_meas_volts() > terminalVmax)
  {
    // Next step is either another step down, or the nearest reasonable step
    current -= config.getINCR_C();
    voltage = terminalVmax + current * config.getCABLE_R();
    charger->setVC(voltage, current);
  }
}

bool ChargeSequence::charge_notOk() {
    //check if battery is OK
    for (size_t i=0;i<num_bats;i++) {
        if (batteries[i]->get_errors()) {
            ROS_ERROR_STREAM("Battery "<<batteries[i]->get_id() << " error, aborting");
            return true;
        }
    
        //Check if we heard from the battery recently enough
        if (batteries[i]->get_time_since() > batteryCommsTimeout) {
            ROS_ERROR_STREAM("Battery "<<batteries[i]->get_id() << " last comm "<<batteries[i]->get_time_since() 
            << " seconds ago, aborting charge");
            return true;
        }
    }
    //Check if power supply is OK
    if (charger->get_errors()) {
        ROS_ERROR_STREAM("Power Supply " << charger->get_addr() << " not OK, aborting charge");
        return true;
    }

    //Check if we heard from power supply recently enough
    if (charger->get_time_since() > chargerCommsTimeout) {
        ROS_ERROR_STREAM("Power supply "<< charger->get_id() << " lost comm " <<charger->get_time_since()
                                            << " seconds ago, aborting charge");
        true;
    }

    return false;
}

void ChargeSequence::stopCharge() {
    for (size_t i=0;i<num_bats;i++){
        batteries[i]->setOff();
    }
    charger->setOff();
    state = DONE;
}

void ChargeSequence::startCharge() {
    //ROS_WARN_STREAM("IN START CHARGE");
    starting = true;
    batteryId = 0;
    for (size_t i=0;i<num_bats;i++) {
        batteries[i]->setOff();
        batteries[i]->setCharge();
    }
    charger->setVC(config.getTARGET_V(), config.getINITIAL_C());
    ROS_WARN_STREAM("setVC: "<<config.getTARGET_V()<<" and current: "<<config.getINITIAL_C());
}

